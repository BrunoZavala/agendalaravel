@extends('layouts.app')

@section('content')
<div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Cuentas de Acceso</div>
                    <div class="card-body">
                    <form action="/usuarios" method="POST">
                            @csrf
                            <div class="form-row">
                              <div class="form-group col-md-6">
                                <label>Nombre</label>
                                <input type="text" class="form-control" required name="name" placeholder="Nombre">
                              </div>
                              <div class="form-group col-md-6">
                                <label>Correo</label>
                                <input type="email" class="form-control" required name="email" placeholder="Ejemplo@gmail.com">
                              </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Contraseña</label>
                                    <input type="password" class="form-control" required name="password" placeholder="contraseña">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Nivel de Acceso</label>
                                    <select class="form-control" required name="tipo" >
                                        <option value="1">Administrador</option>
                                        <option value="2">Supervisor</option>
                                        <option value="3">Usuario</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                   <button type="submit" class="btn btn-primary">Guardar</button>

                                </div>
                            </div>

                          </form>
                    </div>
                </div>

            </div>

        </div>
</div>
@endsection
