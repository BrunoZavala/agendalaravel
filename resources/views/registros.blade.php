@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Registro de Agenda</div>

                <div class="card-body">
                    <table class="table table-striped table-hover">


                        <thead>
                          <tr style="text-align: center">
                            <th scope="col">ID</th>
                            <th scope="col">NOMBRE</th>
                            <th scope="col">APELLIDO</th>
                            <th scope="col">DIRECCION</th>
                            <th scope="col">EMAIL</th>
                            <th scope="col">TELEFONO</th>
                            <th scope="col">ESTADO</th>
                            <th scope="col">IMAGEN</th>
                            @if(Auth::user()->tipo<3)
                            <th scope="col">ACCIONES</th>
                            @endif
                          </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $item)
                          <tr style="text-align: center">
                            <td>{{$loop->iteration}}</td>
                            <td>{{$item->nombre}}</td>
                            <td>{{$item->apellido}}</td>
                            <td>{{$item->direccion}}</td>
                            <td>{{$item->email}}</td>
                            <td>{{$item->telefono}}</td>
                            <td>{!! $item->estado() !!}</td>
                            <td>
                                <button type="button" class="btn btn-sm btn-link" data-toggle="modal" data-target="#{{$item->id}}">
                                        <img src="{{asset('/storage/'.$item->imagen)}}" alt="foto" height="50px">
                                </button>
                            </td>
                            @if(Auth::user()->tipo<3)
                            <td>
                            <div class="btn-group">
                                @if(Auth::user()->tipo==1)
                                <a href="{{url('/registros/borrar/'.$item->id)}}" class="btn btn-danger">Eliminar</a>
                                @endif
                                <a href="{{url('/edit/'.$item->id)}}" class="btn btn-dark">Editar</a>
                            </div>
                            </td>
                            @endif
                          </tr>
                        <div class="modal fade" id="{{$item->id}}" tabindex="-1" role="dialog">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h5 class="modal-title">Modal title</h5>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                      <p>Modal body text goes here.</p>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                      <button type="button" class="btn btn-primary">Save changes</button>
                                    </div>
                                  </div>
                                </div>
                              </div>
                          @endforeach
                    </tbody>
                      </table>

                  <div>
                    @if(Auth::user()->tipo==1)
                      <a class="btn btn-primary" href="/registros/nuevo" role="button">Registrar Usuario</a>
                    @endif
                  </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

