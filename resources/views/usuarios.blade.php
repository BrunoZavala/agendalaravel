@extends('layouts.app')

@section('content')
<div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Registro de Agenda</div>

                    {{-- table table-sm table-bordered --}}
                    <div class="card-body">
                        <table class="table table-striped table-hover">
                            <thead>
                              <tr style="text-align: center">
                                <th scope="col">ID</th>
                                <th scope="col">NOMBRE</th>
                                <th scope="col">EMAIL</th>
                                <th scope="col">NIVEL DE ACCESO</th>
                                @if(Auth::user()->tipo<3)
                                <th scope="col">ACCIONES</th>
                                @endif
                              </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $item)
                              <tr style="text-align: center">
                                <td>{{$loop->iteration}}</td>
                                <td>{{$item->name}}</td>
                                <td>{{$item->email}}</td>
                                <td>{!! $item->tipo() !!}</td>
                                @if(Auth::user()->tipo<3)
                                 <td>
                                 <div class="btn-group">
                                        @if(Auth::user()->tipo==1 )
                                            @if($item->tipo!=1)
                                            <a href="{{url('/usuarios/borrar/'.$item->id)}}" class="btn btn-danger">Eliminar</a>
                                            @endif
                                        @endif
                                <a href="{{url('/editarUsuario/'.$item->id)}}" class="btn btn-dark">Editar</a>
                                </div>
                                </td>

                                @endif
                              </tr>
                              @endforeach
                        </tbody>
                    </table>

                      <div>
                        @if(Auth::user()->tipo==1)
                      <a class="btn btn-primary" href="/register" role="button">Registrar Usuario</a>
                        @endif
                      </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

