@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Registro de Agenda</div>

                <div class="card-body">
                    <form action="{{url('/registros/'.$registro->id)}}" method="POST" enctype="multipart/form-data">
                      @csrf
                      <div class="form-row">
                          <div class="form-group col-md-4">
                            <label>Nombre</label>
                          <input type="text" class="form-control" name="name" value="{{$registro->nombre}}" >
                          </div>

                          <div class="form-group col-md-4">
                              <label>Apellido</label>
                          <input type="text" class="form-control" name="apellido" value="{{$registro->apellido}}">
                            </div>

                            <div class="form-group col-md-4">
                              <label>Direccion</label>
                            <input type="text" class="form-control" name="direccion" value="{{$registro->direccion}}">
                            </div>
                      </div>
                      <div class="form-row">
                          <div class="form-group col-md-4">
                              <label>Email</label>
                            <input type="text" class="form-control" name="email" value="{{$registro->email}}">
                            </div>

                            <div class="form-group col-md-4">
                              <label>Telefono</label>
                            <input type="text" class="form-control" name="telefono" value="{{$registro->telefono}}">
                            </div>

                            <div class="form-group col-md-4">
                                  <label>Estado</label>
                            <select class="form-control" name="estado">
                                      <option value="1" @if($registro->estado==1) selected @endif>ACTIVO</option>
                                      <option value="2" @if($registro->estado==2) selected @endif>INACTIVO</option>
                            </select>
                            </div>
                      </div>
                      <div class="form-row">
                          <div class="form-group col-md-12">
                              <label>Imagen</label>
                              <input type="file" class="form-control" name="imagen" accept=".png,.jpg,.jpeg,.gif">
                          </div>
                      </div>
                      <div class="form-row">
                          <button type="submit" class="btn btn-primary mr-3">Guardar</button>
                          <a href="/registros/"><button type="button" class="btn btn-dark">Cancelar</button></a>
                      </div>
                     </form>
                </div>
                {{-- <div class="card-footer">
                    <a href="{{url('/registros')}}" class="btn btn-success">Contactos</a>
                </div> --}}
            </div>
        </div>
    </div>
</div>
@endsection
