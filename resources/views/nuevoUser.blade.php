@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Registro de Agenda</div>

                <div class="card-body">
                    <form action="/guardar" method="POST" enctype="multipart/form-data">
                      @csrf
                          <div class="form-row">
                              <div class="form-group col-md-4">
                                <label>Nombre</label>
                                <input type="text" class="form-control"  required="required" maxlength="20" size="27" name="name" placeholder="">
                              </div>

                              <div class="form-group col-md-4">
                                  <label>Apellido</label>
                                  <input type="text" class="form-control" required="required" maxlength="20" size="27" name="apellido" placeholder="">
                                </div>

                                <div class="form-group col-md-4">
                                  <label>Direccion</label>
                                  <input type="text" class="form-control" required="required" maxlength="25" size="27" name="direccion" placeholder="">
                                </div>
                          </div>

                              <div class="form-row">
                                  <div class="form-group col-md-4">
                                      <label>Email</label>
                                      <input type="email" class="form-control" required="required" maxlength="30" size="27" name="email" placeholder="Ejemplo@gmail.com">
                                    </div>

                                    <div class="form-group col-md-4">
                                      <label>Telefono</label>
                                      <input type="text" class="form-control" required="required" maxlength="9" size="11" name="telefono"  placeholder="">
                                    </div>

                                    <div class="form-group col-md-4">
                                          <label>Estado</label>
                                          <select class="form-control" required="required" name="estado"  placeholder="">
                                              <option value="1">ACTIVO</option>
                                              <option value="2">INACTIVO</option>
                                          </select>
                                      </div>
                              </div>
                              <div class="form-row">
                                  <div class="form-group col-md-12">
                                      <label>Imagen</label>
                                      <input type="file" class="form-control" required="required" name="imagen" accept=".png,.jpg,.jpeg,.gif">
                              </div>
                              <div class="form-row">
                                  <button type="submit" class="btn btn-primary mr-3">Guardar</button>
                                  <a href="{{url('/registros')}}" class="btn btn-dark ">Cancelar</a>
                              </div>

                         </form>

                  <div>

                  </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
