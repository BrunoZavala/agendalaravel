@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Registro de Agenda
                </div>
                <div class="card-body">
                  <div>
                    <a href="{{url('/registros')}}" class="btn btn-success">Contactos</a>
                    <a href="{{url('/usuarios')}}" class="btn btn-success">Usuarios</a>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
