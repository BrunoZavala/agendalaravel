@extends('layouts.app')

@section('content')
<div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Cuentas de Acceso</div>
                    <div class="card-body">
                    <form action="/editarUsuario/{{$registro->id}}" method="POST">
                            @csrf
                            <div class="form-row">
                              <div class="form-group col-md-6">
                                <label>Nombre</label>
                              <input type="text" class="form-control" required name="name" placeholder="Nombre" value="{{$registro->name}}">
                              </div>
                              <div class="form-group col-md-6">
                                <label>Correo</label>
                                <input type="email" class="form-control" required name="email" placeholder="Ejemplo@gmail.com" value="{{$registro->email}}">
                              </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Nivel de Acceso</label>
                                    <select class="form-control" required name="tipo">
                                        <option value="1" @if($registro->tipo==1) selected @endif>Administrador</option>
                                        <option value="2" @if($registro->tipo==2) selected @endif>Supervisor</option>
                                        <option value="3" @if($registro->tipo==3) selected @endif>Usuario</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-12">
                                   <button type="submit" class="btn btn-primary">Guardar</button>
                                </div>
                            </div>

                          </form>
                    </div>
                </div>

            </div>

        </div>
</div>
@endsection
