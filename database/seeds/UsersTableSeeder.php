<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new User;
        $admin->name = 'Administrador';
        $admin->email = 'admin@agenda.test';
        $admin->password = bcrypt('admin');
        $admin->tipo = 1;
        $admin->save();

        $admin = new User;
        $admin->name = 'Supervisor';
        $admin->email = 'super@agenda.test';
        $admin->password = bcrypt('super');
        $admin->tipo = 2;
        $admin->save();

        $admin = new User;
        $admin->name = 'Usuario';
        $admin->email = 'usuario@agenda.test';
        $admin->password = bcrypt('usuario');
        $admin->tipo = 3;
        $admin->save();

    }
}
