<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;


Route::get('/login','AuthController@showlogin')->name('login');
Route::get('/','HomeController@check');
Route::post('/login','AuthController@login');

Route::middleware(['auth'])->group(function(){
    Route::post('/logout','AuthController@logout');
    Route::get('/edit/{id}','ContactController@edit');
    Route::get('/registros/borrar/{id}','ContactController@destroy');
    //Contactos
    Route::get('/registros','ContactController@index');
    Route::get('/registros/nuevo','ContactController@create');


    Route::post('/registros/{id}','ContactController@update');
    Route::get('/home', 'HomeController@index');
    Route::post('/guardar', 'ContactController@store');

    //home
    Route::get('/register','UserController@register');
    Route::get('/usuarios','UserController@index');

    Route::post('/usuarios','UserController@guardar');

    //Eliminar
    Route::get('/usuarios/borrar/{id}','UserController@destroy');

    //Editar
    Route::get('/editarUsuario/{id}','UserController@edit');
    Route::post('/editarUsuario/{id}','UserController@update');


});







