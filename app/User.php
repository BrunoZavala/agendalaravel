<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;
    protected $fillable = [
        'name', 'email', 'password',
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function tipo()
    {
        if($this->tipo == 1){
            return '<span>ADMINISTRADOR</sapan>';
        }

        elseif($this->tipo == 2){
          return 'SUPERVISOR';
        }

        elseif($this->tipo == 3){
            return 'USUARIO';
          }

    }


}
