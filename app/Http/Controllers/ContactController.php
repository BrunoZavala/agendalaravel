<?php

namespace App\Http\Controllers;

use App\Registro;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;


class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mostrar = Registro::all();
        return view ('registros',['data'=>$mostrar]);



        //$text = 'qwerty';$id
        // $data = $mostrar;
        // $datodato = 'xD';
        // $otrodato = 2345;
        // $masdatos =$text;
        // $masdatos2 =$text;
        // $masdatos3 =$text;
        // $masdatos4 =$text;
       //'datodato'=> 'xD','otrodato'=> 2345,'masdatos'=>$text,'masdatos2'=>$text,'masdatos3'=>$text,'masdatos4'=>$text]);
        // return view ('registros',compact('data','datodato','otrodato','masdatos','masdatos2','masdatos3','masdatos4'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('nuevoUser');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        if(Auth::user()->tipo==1){
        $registro = new Registro;
        $registro->nombre=$request->name;
        $registro->apellido=$request->apellido;
        $registro->direccion=$request->direccion;
        $registro->email=$request->email;
        $registro->telefono=$request->telefono;
        $registro->estado=$request->estado;

        if($request->imagen){
            $registro->imagen = Storage::disk('public')->putFile('pictures', $request->file('imagen'));
        }
        $registro->save();
        }
        return redirect('/registros');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        return view('edit',['registro'=>Registro::find($id)]);


            return back();

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request);
        if(Auth::user()->tipo==1){
        $registro=Registro::find($id);
        $registro->nombre=$request->name;
        $registro->apellido=$request->apellido;
        $registro->direccion=$request->direccion;
        $registro->email=$request->email;
        $registro->telefono=$request->telefono;
        $registro->estado=$request->estado;

        if($request->imagen){
            $registro->imagen = Storage::disk('public')->putFile('pictures', $request->file('imagen'));
        }
        $registro->save();
    }
        return redirect('/registros');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $notaEliminar = new Registro();
        $notaEliminar->destroy($id);

        return redirect('/registros');
    }



}
