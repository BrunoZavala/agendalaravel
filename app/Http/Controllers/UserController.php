<?php

namespace App\Http\Controllers;


use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class UserController extends Controller
{
    public function index()
    {
        $mostrar = User::all();
        //dd($mostrar);
        return view ('usuarios',['data'=>$mostrar]);
    }

    public function guardar(Request $request)
    {
        if(Auth::user()->tipo==1){
        $registro = new User;
        $registro->name=$request->name;
        $registro->email=$request->email;
        $registro->password=bcrypt($request->password);
        $registro->tipo=$request->tipo;
        $registro->save();
        }
        return redirect('/usuarios');
    }

    public function update(Request $request,$id){
        if(Auth::user()->tipo<3){
        $registro =User::find($id);
        $registro->name=$request->name;
        $registro->email=$request->email;
        $registro->tipo=$request->tipo;
        $registro->save();
        }
        return redirect('/usuarios');
    }


    public function edit($id)
    {
        return view('editarUsuario',['registro'=>User::find($id)]);
    }

    public function destroy($id)
    {

        $notaEliminar = new User();
        $notaEliminar->destroy($id);

        return redirect('/usuarios');
    }

    public function register(){

        return view('auth.register');

    }
   

}
