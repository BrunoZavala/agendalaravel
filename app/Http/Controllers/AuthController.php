<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function showlogin()
    {
        return view('auth.login');
    }
    public function login(Request $request)
    {
        $user = User::where('email',$request->email)->first();
        // dd($user);
        if($user){
            if(Hash::check($request->password, $user->password)){
                Auth::loginUsingId($user->getKey());
                return redirect('/home');
            }
            else{
                return redirect('/login');
            }
        }
        else{
            return redirect('/login');
        }
    }
    public function logout(Request $request)
    {
        Auth::logout();
        return redirect('/login');
    }

}
