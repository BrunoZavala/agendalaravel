<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Registro extends Model
{
    public function estado()
    {
        if($this->estado == 1){
            return '<span class="badge badge-success">ACTIVO</span>';
        }
        elseif($this->estado == 2){
          return '<span class="badge badge-danger">INACTIVO</span>';
        }
    }

}
